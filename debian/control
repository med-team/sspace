Source: sspace
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Sascha Steinbiss <satta@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               recode,
               python3,
               asciidoctor,
               bowtie
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/sspace
Vcs-Git: https://salsa.debian.org/med-team/sspace.git
Homepage: https://github.com/nsoranzo/sspace_basic
Rules-Requires-Root: no

Package: sspace
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         python3:any,
         bowtie
Description: scaffolding pre-assembled contigs after extension
 SSAKE-based Scaffolding of Pre-Assembled Contigs after Extension (SSPACE)
 is a script able to extend and scaffold pre-assembled contigs using one or
 more mate pairs or paired-end libraries, or even a combination.
 .
 SSPACE is built based on SSAKE. Code of SSAKE is changed to be able to
 extend and scaffold pre-assembled contigs for multiple paired reads
 libraries.
 .
 This is the free 'basic' version of SSPACE. The non-free 'standard' version is
 available directly from Baseclear.
